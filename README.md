An Android app to make a common task simpler. Developed with a partner in an academic setting using Git version control and Scrum.

**Note:**
Some features are a bit unnecessary, as this app was created in an academic setting for a project with feature requirements.
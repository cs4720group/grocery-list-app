package cs4720.cs.virginia.edu.grocerylist;

import java.util.ArrayList;

public class Tuple {
    private ArrayList<Double> coordinates;
    private String storeName;
    public Tuple(ArrayList<Double> coordinates, String storeName){
        this.coordinates = coordinates;
        this. storeName = storeName;
    }

    public ArrayList<Double> getCoordinates() {
        return coordinates;
    }

    public String getStoreName() {
        return storeName;
    }
}

package cs4720.cs.virginia.edu.grocerylist;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.Toast;

import java.util.Date;

public class GpsService extends Service {
    LocationManager locationManager;
    LocationListener locationListener;
    public static double lat;
    public static double lon;

    public GpsService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, "Service Started", Toast.LENGTH_SHORT).show();

        // Define a listener that responds to location updates
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locationListener = new LocationListener() {

            @Override
            public void onLocationChanged(Location location) {
                doServiceWork(location);
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };

        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);


        return super.onStartCommand(intent, flags, startId);
    }

    public void onCreate(){
        Log.i("GPS", "Service onCreate");
    }

    public void onDestroy(){
        Toast.makeText(this, "Service Stopped", Toast.LENGTH_SHORT).show();
        Log.i("GPS", "Service onCreate");
        _shutdownService();

    }

    private void _shutdownService() {
        locationManager.removeUpdates(locationListener);
        Log.i("GPS", "Service shutdown");
    }

    private void doServiceWork(Location location) {
        String currentDateTimeString = DateFormat.format("MM/dd/yy h:mm:ssaa", new Date()).toString();
        Toast.makeText(this, "Location: " + location.toString() + " / " + currentDateTimeString, Toast.LENGTH_LONG).show();

        lat = location.getLatitude();
        lon = location.getLongitude();



//        //Test TEST test TEST
//        Intent i = new Intent(getApplicationContext(), MapsActivity.class);
//        i.putExtra("gpscoords","value");
//        startActivity(i);

    }

}

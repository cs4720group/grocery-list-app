package cs4720.cs.virginia.edu.grocerylist;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import java.util.ArrayList;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class SaveData extends IntentService {
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    static final String ACTION_SAVE = "cs4720.cs.virginia.edu.grocerylist.action.ACTION_SAVE";
    static final String ACTION_UPDATE = "cs4720.cs.virginia.edu.grocerylist.action.ACTION_UPDATE";
    static final String ACTION_DELETE = "cs4720.cs.virginia.edu.grocerylist.action.ACTION_DELETE";

    // TODO: Rename parameters
    private static final String EXTRA_PARAM1 = "cs4720.cs.virginia.edu.grocerylist.extra.PARAM1";
    private static final String EXTRA_PARAM2 = "cs4720.cs.virginia.edu.grocerylist.extra.PARAM2";

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startAction(Context context, Category cat, String action, String item) {
        Intent intent = new Intent(context, SaveData.class);
        intent.putExtra("cs4720.cs.virginia.edu.grocerylist.Category", cat);
        if (action.equals(ACTION_UPDATE))
            intent.setAction(ACTION_UPDATE);
        else if (action.equals(ACTION_SAVE))
            intent.setAction(ACTION_SAVE);
        else {
            intent.setAction(ACTION_DELETE);
            intent.putExtra("item_name", item);
        }
        context.startService(intent);
    }

    public SaveData() {
        super("SaveData");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            Category param1 = intent.getExtras().getParcelable("cs4720.cs.virginia.edu.grocerylist.Category");
            if (ACTION_SAVE.equalsIgnoreCase(action))
                handleActionSave(param1);
            else if (ACTION_UPDATE.equalsIgnoreCase(action))
                handleActionUpdate(param1);
            else {
                String param2 = intent.getExtras().getString("item_name");
                handleActionDelete(param1, param2);
            }
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionSave(Category cat) {
        Log.i("SAVING", "began");
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPref.edit();

        Log.i("SAVING", "began saving to editor");
        // Commit to memory
        editor.putString(cat.getName(), cat.toString());
        editor.commit();
        Log.i("SAVING", cat.toString() + " SAVED");
    }

    /**
     * Handle action Baz in the provided background thread with the provided
     * parameters.
     */
    private void handleActionUpdate(Category cat) {
        Log.i("UPDATING", "began");
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPref.edit();

        // Delete existing object
        editor.remove(cat.getName());
        editor.commit();

        // Commit to memory
        editor.putString(cat.getName(), cat.toString());
        editor.commit();
        Log.i("UPDATING", cat.toString() + " SAVED");
    }

    private void handleActionDelete(Category cat, String item) {
        Log.i("DELETING", "began");
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPref.edit();

        // Delete existing object
        editor.remove(cat.getName());
        editor.commit();

        // Commit to memory (if category has more elements or is the uncategorized category)
        if (cat.getElements().size() > 0 || cat.getName().equalsIgnoreCase("uncategorized")) {
            editor.putString(cat.getName(), cat.toString());
            editor.commit();
            Log.i("DELETING", cat.toString() + " SAVED");
        }
    }
}

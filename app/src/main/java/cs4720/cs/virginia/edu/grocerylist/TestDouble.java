package cs4720.cs.virginia.edu.grocerylist;


import java.text.DecimalFormat;

public class TestDouble{

    private static DecimalFormat df2 = new DecimalFormat(".##");

    public static void main(String[] args) {

        double input = 32.123456;
        System.out.println("double : " + input);
        System.out.println("double : " + df2.format(input));

    }

}
package cs4720.cs.virginia.edu.grocerylist;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
    LatLng currentPosition;
    LocationManager locationManager;
    LocationListener locationListener;
    ArrayList<Tuple> nearestStores = new ArrayList<Tuple>();
    ArrayList<Marker> groceryMarkers = new ArrayList<Marker>();
    ArrayList<String> groceryNames = new ArrayList<String>();
    boolean firsttime = true;

    double lat, lon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.onActivityCreateSetTheme(this);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
//        Toast.makeText(getApplicationContext(), "Please wait 20-30 seconds for grocery store locations to load.", Toast.LENGTH_LONG).show();
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            int value = extras.getInt("CATEGORY QUANTITY");
            Toast.makeText(this, "You have " + value + " categories on your list.", Toast.LENGTH_SHORT).show();
        }



    }

    @Override
    protected void onDestroy() {
        locationManager.removeUpdates(locationListener);
        Log.i("GPS", "GPS destroyed");
        super.onDestroy();
    }

    @Override
    public void onMapReady(GoogleMap map) {
        final GoogleMap m = map;
        m.setMyLocationEnabled(true);


        locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Log.i("GPS", "Location changed");
                String currentDateTimeString = DateFormat.format("MM/dd/yy h:mm:ssaa", new Date()).toString();
                lat = location.getLatitude();
                lon = location.getLongitude();
                LatLng flag = new LatLng(lat, lon);

                if (firsttime) {
                    m.moveCamera(CameraUpdateFactory.newLatLng(flag));
                    m.animateCamera(CameraUpdateFactory.zoomTo(14));
                }
                firsttime = false;


                if(!groceryMarkers.isEmpty()){
                    for(Marker ma : groceryMarkers){
                        Log.i("FLAGS", ma.toString());
                        ma.remove();
                    }
                }

                groceryMarkers.clear();

                for(int i = 0; i < nearestStores.size(); i++){
                    LatLng ll = new LatLng(nearestStores.get(i).getCoordinates().get(0), nearestStores.get(i).getCoordinates().get(1));
                    String storeName = nearestStores.get(i).getStoreName();
                    Marker newmark = m.addMarker(new MarkerOptions().position(ll).title(storeName).icon(BitmapDescriptorFactory.fromResource(R.mipmap.grocery_icon)));
                    groceryMarkers.add(newmark);
                    Log.i("CORDS", nearestStores.get(i).toString() + " " + i);
                }

                new GetStores().execute();

            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
    }

    private class GetStores extends AsyncTask{
        String returnedJSON;

        @Override
        protected String doInBackground(Object params[]) {
            try {

                String url = "https://maps.googleapis.com/maps/api/place/search/json?location=" + lat + "," + lon + "&radius=8000&types=grocery_or_supermarket&sensor=false&key=AIzaSyBqUdDueMea5MuSVr5OSaVKH6ff6xsnrxY";

                returnedJSON = makeCall(url);

            } catch (IOException e) {
                e.printStackTrace();
            }
            return "";
        }

        private ArrayList<Tuple> parse(String returnedJSON) {
            ArrayList<Tuple> geoList = new ArrayList<Tuple>();
            try {
                JSONObject jsonObj = new JSONObject(returnedJSON);
                JSONArray allStores = jsonObj.getJSONArray("results");

                for (int i = 0; i < allStores.length(); i++) {
                    double placeLat = jsonObj.getJSONArray("results").getJSONObject(i).getJSONObject("geometry").getJSONObject("location").getDouble("lat");
                    double placeLon = jsonObj.getJSONArray("results").getJSONObject(i).getJSONObject("geometry").getJSONObject("location").getDouble("lng");
                    String name = jsonObj.getJSONArray("results").getJSONObject(i).getString("name");
                    ArrayList<Double> coords = new ArrayList<Double>();
                    coords.add(placeLat);
                    coords.add(placeLon);
                    Tuple t = new Tuple(coords, name);
                    geoList.add(t);

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return geoList;
        }

        @Override
        protected void onPostExecute(Object o) {
            nearestStores = parse(returnedJSON);
        }
    }

    public String makeCall(String url) throws IOException {
        OkHttpClient client = new OkHttpClient();
        Request req = new Request.Builder().url(url).build();
        Response response = client.newCall(req).execute();
        return response.body().string();
    }

    public void back(View view){
        finish();
    }
}

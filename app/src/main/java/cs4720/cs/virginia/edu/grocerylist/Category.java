package cs4720.cs.virginia.edu.grocerylist;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.Externalizable;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**
 * Created by Levi on 9/22/2015.
 */
public class Category implements Parcelable {
    private ArrayList<GroceryItem> elements;
    private String name;
    private TextView tView;
    private int unchecked;


    public Category(String name) {
        this.elements = new ArrayList<GroceryItem>();
        this.name = name;
        this.unchecked = 0;
    }

    public Category(String name, ArrayList<GroceryItem> elements) {
        this.elements = elements;
        this.name = name;

        //TODO: Update unchecked with the number of incomplete items from the given ArrayList
        unchecked = 0;
    }

    /**
     * Turns a String into a Category object
     * @param serialized The string. *Must be created by Category.toString() method or this won't work*
     * @param convert Unused parameter to differentiate from main constructor
     */
    public Category(String serialized, boolean convert) { // Added boolean parameter to differentiate from main constructor
        Scanner main = new Scanner(serialized).useDelimiter(" ");
        String n = main.next();
        this.name = n.replace("`", " ");
        this.unchecked = Integer.valueOf(main.next());
        this.elements = new ArrayList<GroceryItem>();
        while (main.hasNext()) {
            String text = main.next();
            GroceryItem item = new GroceryItem(text, true);
            elements.add(item);
        }
    }

    public void addElement(GroceryItem item) {
        Log.i("CATEGORY: " + name, "Element added");
        elements.add(item);
    }

    public ArrayList<GroceryItem> getElements() {
        return elements;
    }

    public void setElements(ArrayList<GroceryItem> elements) {
        this.elements = elements;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TextView getTextView() {
        return tView;
    }

    public void setTextView(TextView tView) {
        this.tView = tView;
    }

    public int getUnchecked() {
        return unchecked;
    }

    public void incUnchecked() {
        this.unchecked++;
    }

    public void decUnchecked() {
        this.unchecked--;
    }

    public void setUnchecked(int unchecked) {
        this.unchecked = unchecked;
    }

    public String toString() {
        String n = this.name;
        n = n.replace(" ", "`");
        String value = n + " " + unchecked + " ";
        for (int i = 0; i < elements.size(); i++) {
            GroceryItem item = elements.get(i);
            if (i == elements.size()-1)
                value += item.toString();
            else
                value += item.toString() + " ";
        }
        return value ;
    }

    // All code below this is for Parcelable implementation

    Category(Parcel in) {
        this.name = in.readString();
        this.unchecked = in.readInt();
        this.elements = new ArrayList<GroceryItem>();
        in.readTypedList(elements, GroceryItem.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeInt(unchecked);
        dest.writeTypedList(elements);
    }

    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<Category> CREATOR = new Parcelable.Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel in) {
            return new Category(in);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };
}

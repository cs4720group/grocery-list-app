package cs4720.cs.virginia.edu.grocerylist;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ListActivity extends Activity {
    private final String TAG = getClass().getSimpleName();
    private static DecimalFormat df2 = new DecimalFormat("#.00");

    /**
     * Identifies whether delete mode is active or not
     */
    private boolean deleteOn;

    boolean firsttime = true;

    /**
     * Items in this list (and corresponding GroceryItems get deleted after delete mode is exited
     */
    private ArrayList<CheckBox> deleteItems;

    /**
     * List of categories
     */
    private ArrayList<Category> categories;
    /**
     * Default category for uncategorized elements
     */
    private Category uncategorized;
    /**
     * Layout where unchecked items reside
     */
    private LinearLayout unchecked;
    /**
     * Layout where checked items reside
     */
    private LinearLayout checked;
    /**
     * Divider line to separate checked items from unchecked items
     */
    private View checked_div;
    /**
     * Checked TextView that indicates the checked section where checked items reside
     */
    private TextView checked_text;

    private View floating_delete;

    private View floating_cancel;
    /**
     * Number of checked items
     */
    private int num_checked;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;
    private List<String> catNames;
    private ArrayAdapter<String> autoAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.onActivityCreateSetTheme(this);
        setContentView(R.layout.activity_list);

        // SharedPreferences setup
        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        editor = sharedPref.edit();

        // Prevents keyboard from popping up right away
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        // UI references
        unchecked = (LinearLayout) findViewById(R.id.unchecked);
        checked = (LinearLayout) findViewById(R.id.checked);
        checked_div = findViewById(R.id.checked_div);
        checked_text = (TextView) findViewById(R.id.checked_text);
        floating_delete = findViewById(R.id.floating_delete);
        floating_cancel = findViewById(R.id.floating_cancel);

        addDeleteButtonListener(floating_delete);
        addDeleteButtonListener(floating_cancel);

        // Instantiate deleteItems
        deleteItems = new ArrayList<CheckBox>();

        if (savedInstanceState == null) {
            // Keep track of number of checked items
            num_checked = 0;

            // Initialize list of categories
            categories = new ArrayList<Category>();

            // Retrieve stored items from SharedPreferences
            Map<String, ?> all_cats = sharedPref.getAll();
            if (!all_cats.isEmpty()) { // SharedPreferences has some things stored
                for (Map.Entry<String, ?> entry : all_cats.entrySet()) {
                    String value = entry.getValue().toString();
                    Category cat = new Category(value, true);
                    Log.i("CATEGORY RETRIEVED", entry.getValue().toString());
                    categories.add(cat);
                }

                Log.i("LISTACTIVITY", "Categories: " + categories.toString());
                boolean uncat_exists = false;
                for (Category c : categories) {
                    if (c.getName().equalsIgnoreCase("uncategorized"))
                        uncat_exists = true;
                }

                if (!uncat_exists) {
                    createUncategorized();
                }
                // Rebuild list
                constructList();
            } else {
                createUncategorized();
            }
        }
        else {
//            num_checked = savedInstanceState.getInt("num_checked", 0);
            num_checked = 0;
            categories = savedInstanceState.getParcelableArrayList("categories");
            constructList();
        }
        catNames = new ArrayList<>();
        for (Category c : categories) {
            catNames.add(c.getName());
        }
        autoAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, catNames);
        AutoCompleteTextView textView = (AutoCompleteTextView)
                findViewById(R.id.cat_box);
        textView.setAdapter(autoAdapter);
        textView.setThreshold(1);
    }

    public static void onActivityCreateSetTheme(Activity activity, int theme) {
        if (theme == Utils.THEME_DARK)
            Utils.changeToTheme(activity, Utils.THEME_DARK);
        else
            Utils.changeToTheme(activity, Utils.THEME_DARK);
    }

    private void createUncategorized() {
        // Create default uncategorized category (is that a paradox?), if one wasn't already created
        uncategorized = new Category("Uncategorized");

        if (categories.isEmpty()) {
            TextView uncat_view = new TextView(this); // Textview that will represent the uncategorized category
            uncat_view.setText("Uncategorized");
            uncat_view.setTypeface(null, Typeface.BOLD);
            uncategorized.setTextView(uncat_view);
            unchecked.addView(uncat_view);
            uncat_view.setVisibility(View.GONE); // Make invisible until it contains elements
            uncat_view.setPadding(0, 5, 0, 0);
        }
        categories.add(0, uncategorized);
    }

    private void constructList() {
        Log.i("LISTACTIVITY", "Categories size: " + categories.size());
        for (int i = 0; i < categories.size(); i++) {
            if (categories.get(i).getName().toLowerCase().equals("uncategorized")) {
                this.uncategorized = categories.get(i);
            }
            Category cat = categories.get(i);
            Log.i("CATEGORY BEING ADDED", cat.toString());
            ArrayList<GroceryItem> items = cat.getElements();
            TextView tv = new TextView(this);
            tv.setText(cat.getName());
            cat.setTextView(tv);

            unchecked.addView(tv);

            if (cat.getElements().size() != 0) {
                for (int j = 0; j < items.size(); j++) {
                    GroceryItem item = items.get(j);
                    CheckBox cb = new CheckBox(this);
                    if (item.getPrice() != 0.00)
                        cb.setText(item.getItem() + "\t\t\t\t\t\t" + "$" + df2.format(item.getPrice()));
                    else
                        cb.setText(item.getItem());
                    cb.setTag(cat);
                    addCheckboxListener(cb);
                    if (item.getIsDone()) {
                        checked.addView(cb);
                        cb.setChecked(true);
                        this.num_checked++;
                        if (this.num_checked == 1) {
                            checked_div.setVisibility(View.VISIBLE);
                            checked_text.setVisibility(View.VISIBLE);
                        }
                    } else {
                        unchecked.addView(cb);
                    }
                }
            }
            if (cat.getUnchecked() == 0)
                tv.setVisibility(View.GONE);
            tv.setTypeface(Typeface.DEFAULT_BOLD);
            tv.setPadding(0, 5, 0, 0);
            if (num_checked > 0) {
                checked_div.setVisibility(View.VISIBLE);
                checked_text.setVisibility(View.VISIBLE);
            }
        }
    }

    public void addDeleteButtonListener(View v) {
        v.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                boolean canceled = v.getId() == R.id.floating_cancel;
                offDeleteMode(canceled);
            }
        });
    }

    public void addCheckboxListener(CheckBox cb) {
        cb.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                CheckBox cb = (CheckBox) v;
                Category cat = (Category) cb.getTag();
                Log.d("ListActivity", "checkbox touched");

                if (deleteOn) {
                    if (deleteItems.contains(cb)) {
                        deleteItems.remove(cb);
                        cb.setBackgroundColor(Color.TRANSPARENT);
                    } else {
                        deleteItems.add(cb);
                        cb.setBackgroundColor(Color.parseColor("#E0E0E0"));
                    }
                    if (cb.isChecked())
                        cb.setChecked(false);
                    else
                        cb.setChecked(true);
                } else {

                    if ((cb.isChecked())) {
                        unchecked.removeView(cb);
                        checked.addView(cb);
                        cat.decUnchecked(); // Decrement the item's category's count of its unchecked items
                        if (cat.getUnchecked() == 0)
                            cat.getTextView().setVisibility(View.GONE); // If category has no items, make it invisible
                        num_checked++; // increment number of checked items
                        if (num_checked > 0) { // Set checked dividers to visible if checked items == 1
                            checked_div.setVisibility(View.VISIBLE);
                            checked_text.setVisibility(View.VISIBLE);
                        }
                        // Set Grocery Item to done
                        String cb_text = cb.getText().toString();
                        int end_index = 0;
                        boolean done = false;
                        int j = 0;
                        while (j < cb_text.length() && !done) {
                            String letter = cb_text.substring(j, j + 1);
                            if (letter.equals("\t")) {
                                end_index = j;
                                done = true;
                            }
                            j++;
                        }
                        if (end_index != 0)
                            cb_text = cb_text.substring(0, end_index);
                        Log.d(TAG, cb_text + ": checked");
                        boolean somethingSetAsDone = false;
                        for (int i = 0; i < cat.getElements().size(); i++) {
                            GroceryItem item = cat.getElements().get(i);
                            if (item.getItem().equalsIgnoreCase(cb_text)) {
                                item.setIsDone(true);
                                somethingSetAsDone = true;
                                Log.d(TAG, item.getItem() + " set to done = true");
                            }
                        }

                        if (!somethingSetAsDone) {
                            Log.e(TAG, "Something not set as done! cb_text = " + cb_text + "; cat = " + cat.toString());
                        }
                    } else {
                        checked.removeView(cb);
                        unchecked.addView(cb, unchecked.indexOfChild(((Category) cb.getTag()).getTextView()) + ((Category) cb.getTag()).getUnchecked() + 1);
                        cat.incUnchecked(); // Increment the item's category's count of its unchecked items
                        if (cat.getUnchecked() == 1)
                            cat.getTextView().setVisibility(View.VISIBLE); // If category now has an element, make it visible
                        num_checked--; // decrement number of checked items
                        if (num_checked <= 0) { // Set checked dividers to be invisible if checked items == 0
                            checked_div.setVisibility(View.GONE);
                            checked_text.setVisibility(View.GONE);
                        }
                        // Set Grocery Item to not done
                        String cb_text = cb.getText().toString();
                        int end_index = 0;
                        boolean done = false;
                        int j = 0;
                        while (j < cb_text.length() && !done) {
                            String letter = cb_text.substring(j, j + 1);
                            if (letter.equals("\t")) {
                                end_index = j;
                                done = true;
                            }
                            j++;
                        }
                        if (end_index != 0)
                            cb_text = cb_text.substring(0, end_index);
                        Log.d(TAG, cb_text + ": unchecked");
                        boolean somethingSetAsNotDone = false;
                        for (int i = 0; i < cat.getElements().size(); i++) {
                            GroceryItem item = cat.getElements().get(i);
                            if (item.getItem().equalsIgnoreCase(cb_text)) {
                                item.setIsDone(false);
                                Log.d(TAG, item.getItem() + " set to done = true");
                                somethingSetAsNotDone = true;
                            }
                        }
                        if (!somethingSetAsNotDone) {
                            Log.e(TAG, "Something not set as not done! cb_text = " + cb_text + "; cat = " + cat.toString());
                        }
                    }
                    updateCategory(cat, SaveData.ACTION_UPDATE, "");
                }
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList("categories", categories);
        outState.putInt("num_checked", num_checked);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_location) { // Allows GPS button in action bar to function
            to_map();
        }

        else if(id == R.id.action_light){
            to_sensor();
        }
        else if (id == R.id.action_delete) {
            this.deleteOn = !this.deleteOn;
            Log.d("ListActivity", "deleteOn: " + deleteOn);
            if (deleteOn)
                onDeleteMode();
            else
                offDeleteMode(true);
        }

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void to_map() {
        Intent intent = new Intent(this, MapsActivity.class);
        intent.putExtra("CATEGORY QUANTITY", categories.size()); // Puts information of # of categories in intent to MapsActivity (per requirement of Milestone II
        startActivity(intent);
    }

    public void onDeleteMode() {
        this.deleteOn = true;
        floating_cancel.setVisibility(View.VISIBLE);
        floating_delete.setVisibility(View.VISIBLE);
        Toast.makeText(this, "You have now entered delete mode", Toast.LENGTH_SHORT).show();
    }

    public void offDeleteMode(boolean canceled) {
        this.deleteOn = false;
        if (canceled) {
            for (int i = 0; i < deleteItems.size(); i++) {
                deleteItems.get(i).setBackgroundColor(Color.TRANSPARENT);
            }
        }
        else {
            for (int i = 0; i < deleteItems.size(); i++) {
                Category cat = (Category)((CheckBox)deleteItems.get(i)).getTag();
                CheckBox cb = deleteItems.get(i);
                String name = deleteItems.get(i).getText().toString();
                int end_index = 0;
                boolean done = false;
                int j = 0;
                while (j < name.length() && !done) {
                    String letter = name.substring(j, j + 1);
                    if (letter.equals("\t")) {
                        end_index = j;
                        done = true;
                    }
                    j++;
                }
                if (end_index != 0)
                    name = name.substring(0, end_index);

                String dItems = "";
                for (CheckBox c : deleteItems) {
                    dItems += " " + c.getText();
                }
                Log.d("ListActivity", "deleteItems: " + dItems);

                // Delete item from memory
                ArrayList<GroceryItem> items = cat.getElements();
                for (int k = 0; k < items.size(); k++) {
                    GroceryItem current = items.get(k);
                    if (current.getItem().equalsIgnoreCase(name)) {
                        if (!items.get(k).getIsDone())
                            cat.decUnchecked();
                        items.remove(k);
                    }
                }
                cat.setElements(items);

                deleteItems.get(i).setVisibility(View.GONE);
                Log.d("ListActivity", "checkbox checked? : " + cb.isChecked());
                if (!cb.isChecked()) {
                    Log.d("ListActivity", "category decremented: " + cat.getUnchecked());
                    if (cat.getUnchecked() <= 0)
                        cat.getTextView().setVisibility(View.GONE);
                }
                else {
                    Log.d("ListActivity", "category not decremented");
                    this.num_checked--;
                    if (num_checked <= 0) { // Set checked dividers to be invisible if checked items == 0
                        checked_div.setVisibility(View.GONE);
                        checked_text.setVisibility(View.GONE);
                    }
                }
                updateCategory(cat, SaveData.ACTION_DELETE, name);
            }
        }
        deleteItems.clear();
        floating_cancel.setVisibility(View.GONE);
        floating_delete.setVisibility(View.GONE);
    }

    public void addItem(View view) {
        boolean has_cat = false;
        boolean has_price = false;
        String cat_text = "";
        String price_text = "";

        // Get user input from name box
        EditText add = (EditText) findViewById(R.id.add_box);
        String add_text = add.getText().toString();
        add.setText("");

        // Get user input from category box (if any exists)
        AutoCompleteTextView cat = (AutoCompleteTextView) findViewById(R.id.cat_box);
        if (!cat.getText().toString().isEmpty()) {
            cat_text = cat.getText().toString();
            cat.setText("");
            has_cat = true;
        }

        // Get user input from price box (if any exists)
        EditText price = (EditText) findViewById(R.id.price_box);
        if (!price.getText().toString().isEmpty()) {
            price_text = price.getText().toString();
            price.setText("");
            has_price = true;
        }

        Category category = uncategorized;
        Log.d("ListActivity", "Uncategorized: " + category.toString());
        TextView cat_view = category.getTextView();

        Boolean found = false;
        if (add_text.isEmpty()) {
            Toast.makeText(this, "Must enter item name", Toast.LENGTH_SHORT).show();
        } else {
            // Create new grocery list item
            GroceryItem item = new GroceryItem(add_text);
            if (has_price) {
                item.setPrice(Double.valueOf(price_text));
            }
            if (has_cat) {
                int i = 0;
                while (i < categories.size() && !found) {

                    if (categories.get(i).getName().equals(cat_text)) {
                        found = true;
                        category = categories.get(i);
                    }

                    i++;
                }
                if (!found) {
                    cat_view = new TextView(this);
                    cat_view.setText(cat_text);
                    cat_view.setTypeface(null, Typeface.BOLD);
                    category = new Category(cat_text);
                    category.setTextView(cat_view);
                    categories.add(category);
                    autoAdapter.add(category.getName());

                    unchecked.addView(cat_view, 0);
                    cat_view.setPadding(0, 5, 0, 0);
                }
            }

            // Create new checkbox and set text to user input
            CheckBox newBox = new CheckBox(this);
            addCheckboxListener(newBox);

            if (has_price)
                newBox.setText(add_text + "\t\t" + '$' + df2.format(Double.parseDouble(price_text)));
            else
                newBox.setText(add_text);

            category.incUnchecked();
            category.addElement(item);

            int index = (unchecked.indexOfChild(category.getTextView()) + category.getUnchecked());
            newBox.setTag(category);
            unchecked.addView(newBox, index);


            category.getTextView().setVisibility(View.VISIBLE);
            String action;
            if (found)
                action = SaveData.ACTION_UPDATE;
            else
                action = SaveData.ACTION_SAVE;
            updateCategory(category, action, "");
        }
    }

    public void updateCategory(Category cat, String action, String item) {
        SaveData sd = new SaveData();
        sd.startAction(this, cat, action, item);
    }

    public void to_sensor(){
        Intent intent = new Intent(this, SensorActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onResume() {

        if(firsttime == true){
            super.onResume();
            firsttime = false;
        }
        else{
            super.onResume();
            recreate();

        }
    }
}

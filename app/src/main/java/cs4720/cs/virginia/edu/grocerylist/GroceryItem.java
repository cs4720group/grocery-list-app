package cs4720.cs.virginia.edu.grocerylist;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Scanner;

public class GroceryItem implements Parcelable, Comparable<GroceryItem> {
    private String item;
    private double price;
    private boolean isDone;

    public GroceryItem(String item){
        this.item = item;
        this.price = 0.00;
        this.isDone = false;
    }

    /**
     * Converts String to GroceryItem object
     * @param serialized String to be converted. *Must come from GroceryItem.toString()*
     * @param convert Unusued variable to differentiate from default constructor
     */
    public GroceryItem(String serialized, boolean convert) {
        Scanner main = new Scanner(serialized).useDelimiter(",");
        String name = main.next();
        this.item = name.replace("`", " ");
        this.price = Double.valueOf(main.next());
        this.isDone = Boolean.parseBoolean(main.next());
    }

    public GroceryItem(String item, double price){
        this.item = item;
        this.price = price;
        this.isDone = false;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean getIsDone() {
        return isDone;
    }

    public void setIsDone(boolean isDone) {
        this.isDone = isDone;
    }

    public String toString() {
        String name = this.item.replace(" ", "`");
        String value = name + "," + this.price + "," + this.isDone;
        return value;
    }

    // All code below this is implementation of Parcelable

    GroceryItem(Parcel in) {
        this.item = in.readString();
        this.price = in.readDouble();
        this.isDone = (in.readInt() == 1);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(item);
        dest.writeDouble(price);
        dest.writeInt(isDone ? 1 : 0);
    }

    public static final Parcelable.Creator<GroceryItem> CREATOR = new Parcelable.Creator<GroceryItem>() {
        public GroceryItem createFromParcel(Parcel in) {
            return new GroceryItem(in);
        }

        public GroceryItem[] newArray(int size) {
            return new GroceryItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public int compareTo(GroceryItem another) {
        int result;
        if (this.isDone && another.getIsDone())
            return 0;
        else if (this.isDone)
            return -1;
        else
            return 1;
    }
}
